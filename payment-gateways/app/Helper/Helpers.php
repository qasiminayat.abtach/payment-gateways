<?php


function storeTransaction($transactionArray,$customerArray){

    $transaction = new \App\Models\Transaction();
    $transaction->token = $transactionArray['token'];
    $transaction->amount = $transactionArray['amount'];
    $transaction->currency = $transactionArray['currency'];
    $transaction->status =  $transactionArray['status'];
    $transaction->first_4 = $transactionArray['first_4'];
    $transaction->last_6 = $transactionArray['last_6'];
    $transaction->fees = $transactionArray['fees'];
    $transaction->save();

    $customer = new App\Models\Customer();
    $customer->name = $customerArray['name'];
    $customer->email = $customerArray['email'];
    $customer->phone =  $customerArray['phone'];
    $customer->country = $customerArray['country'];
    $customer->state = $customerArray['state'];
    $customer->city = $customerArray['city'];
    $customer->zip = $customerArray['zip'];
    $customer->address = $customerArray['address'];
    $customer->merchant_id = $customerArray['merchant_id'];
    $customer->transaction_id = $transaction->id;
    $customer->save();


    if(!empty($customer) && !empty($transaction)){
        return true;
    }
    else{
        return false;
    }

}

function storeTransactionAttempt($transactionArray,$customerArray){

    $attempt = new \App\Models\TransactionAttempt();
    $attempt->token = $transactionArray['token'];
    $attempt->amount = $transactionArray['amount'];
    $attempt->currency = $transactionArray['currency'];
    $attempt->status =  $transactionArray['status'];
    $attempt->first_4 = $transactionArray['first_4'];
    $attempt->last_6 = $transactionArray['last_6'];
    $attempt->fees = $transactionArray['fees'];
    $attempt->name = $customerArray['name'];
    $attempt->email = $customerArray['email'];
    $attempt->phone =  $customerArray['phone'];
    $attempt->country = $customerArray['country'];
    $attempt->state = $customerArray['state'];
    $attempt->city = $customerArray['city'];
    $attempt->zip = $customerArray['zip'];
    $attempt->address = $customerArray['address'];
    $attempt->merchant_id = $customerArray['merchant_id'];
    $attempt->save();


    if(!empty($attempt)){
        return true;
    }
    else{
        return false;
    }
}



