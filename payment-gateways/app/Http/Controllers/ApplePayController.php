<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Pay;
use Omnipay\Omnipay;

class ApplePayController extends Controller
{
    public function applePay(){

        return view('payment.apple.apple-pay');
    }


    public function aliPay(){

        $order = [
            'out_trade_no' => time (),
            'total_amount' => '1' ,
            'subject' => 'test subject-test' ,
       ];

       return Pay::alipay()->web($order);


        // $gateway = Omnipay::create('Alipay_AopPage');
        // $gateway->setSignType('RSA2'); // RSA/RSA2/MD5. Use certificate mode must set RSA2
        // $gateway->setAppId('5Y0A7N2Y4L4U05645');
        // $gateway->setPrivateKey('MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCsSyva/HrmQXML/sErr4zPnuHi9miN5+eHVm0s7BLAqT9WMoecrr+xRjmZlopC4ljuBQpESVklqaMuapAPRn8Guff0z47UQOXbv1WVkZsUcuYY8GVzh40RaAU7H9fNYYN6ZHQI8lww5P1d/lDfzcCVZ023aYtuLAkVKqpl0xSntfwHrzu1bfli50XvKvcErigReAVtCQ5kHJbYWmDfU8vvFeGl0KjfJxxP/ekbkEfy0bytqKgwT0c2AsrYuLoK/nK4mhcxJXnssLBEQM5qe4a3FLI2mWKFUxhDBYLeVMduXl+m7RARxe/5CVfCYem4ghUTzRO2hm5XfFDy0NzlTV+lAgMBAAECggEBAJ+BpGNDNYBv/VOQkUOhbWpVfGiDmT4vARExeTeKgr3ssl9ZvmTW/07eZbsEPJI5PP12mw+nHCobB6CUNTR/HmCa0KzLbz6fqm2rkE89EuZ2jQFvpWy0hGwl8hnPnBle+rrXvvj9b04w4UQgirz3niBzF+cChnwDtpDrA//q0AeGj/Xz7pfypWXM5G0ZLNcO9pYulyhfkVxKvy2QtiikmT1EoDNcxZmw6EoZPJLdKKX0Zwh7F9fWOyV1xuj8QbR3/Fp0n/aMmICf2cjsdHddg6XiuBjKNloQqh7P+C2OEVxZRmOFG+vGSPygb0AwM/lMb4gNrB92tMaTwpsAzvfyDiECgYEA+R9hDv6cXldN+I5Q/m4Ezo0Pt5WwXhrhEzd7rAlvdoYvhv9oSqUElJye6xZ1V7CeZOD1TuPgO/+7iYRQzz7s3qY+/S4fWbrUdtlnItHVYKZ1mUNIdnkUVmR4kEjO1JRVxzdzvjDaza6IcgH+XQ4l5dOxUdgr6DL+yVYXG2SBpxkCgYEAsQzR6sIHTtVuAsjfXRaGdQ1Q2f7CrOLIQKL50i8KBoaKj3WXDQ7FRSUQLBLYMvUaxQN0VOTU78vKfN2XxAstuwf+Rjz6edeXF8ghY5TTjY4h87EIfxpyhx8X64bxwvXevrOVqMbA5ShKlAUxFM34N2rAn39L/MOMe3hOhpOsSm0CgYAbcuap1Un7ZxkzOtMXxZ2H+KjzrG2bYNDN4j4hLYT3hT91W2ztVJNVoJAuDPJu8AnzyYmd4KPTWJEcTNTukNteiOjF0rAnndtNIRUpE/+zv4OTmINCcoLYJgfWtf78E0AETErXeOCS6xkjnq6dKDLqyNFv2Ca2VfAW1QKExSU3+QKBgQCs9Q8kPIOTGaCsi7rYXybetjFjpCJdbfWeXxeZomYy1cAKQR9cnC9OStSvmYEm1pZeG+/K9vilgPnAXWDjHOCBs19NEkjGOjvZ+aHUfBDmWhEE1M6uDBIRuehOJ7eJc1M0DI+JHxBVTLT+QMwfvM5cpKajqWF7/TQBcheUFDLdSQKBgC2dF3fKcx2ZKtvAjgS+1CtTIQNccw3ZbacwSK1Zty7e8PHywI/aY/NIAvFkxCghy0GWzuhg8Z/Jxw61UViM4ediuxx03LiNC2q8i1UUjwJNf4QKCKdibm0H6EhaAtgsY+w+6uZCSbQVMxUhFS31mwmlEcmr31YyQvLyItGPCh1b');
        // $gateway->setAlipayPublicKey('MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArEsr2vx65kFzC/7BK6+Mz57h4vZojefnh1ZtLOwSwKk/VjKHnK6/sUY5mZaKQuJY7gUKRElZJamjLmqQD0Z/Brn39M+O1EDl279VlZGbFHLmGPBlc4eNEWgFOx/XzWGDemR0CPJcMOT9Xf5Q383AlWdNt2mLbiwJFSqqZdMUp7X8B687tW35YudF7yr3BK4oEXgFbQkOZByW2Fpg31PL7xXhpdCo3yccT/3pG5BH8tG8raioME9HNgLK2Li6Cv5yuJoXMSV57LCwREDOanuGtxSyNplihVMYQwWC3lTHbl5fpu0QEcXv+QlXwmHpuIIVE80TtoZuV3xQ8tDc5U1fpQIDAQAB'); // Need not set this when used certificate mode
        // $gateway->setReturnUrl('http://payment-integraion.local');
        // $gateway->setNotifyUrl('http://payment-integraion.local/ali-pay');

        // // Must set cert path if you used certificate mode
        // //$gateway->setAlipayRootCert('the_alipay_root_cert'); // alipayRootCert.crt
        // //$gateway->setAlipayPublicCert('the_alipay_public_cert'); // alipayCertPublicKey_RSA2.crt
        // //$gateway->setAppCert('the_app_public_cert'); // appCertPublicKey.crt
        // //$gateway->setCheckAlipayPublicCert(true);

        // /**
        //  * @var AopTradePagePayResponse $response
        //  */
        // $response = $gateway->purchase()->setBizContent([
        //     'subject'      => 'test',
        //     'out_trade_no' => date('YmdHis') . mt_rand(1000, 9999),
        //     'total_amount' => '0.01',
        //     'product_code' => 'FAST_INSTANT_TRADE_PAY',
        // ])->send();


        // $url = $response->getRedirectUrl();
        // dd($url);

    }
}
