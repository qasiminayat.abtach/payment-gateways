<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Payment;
use Sample\PayPalClient;
use PayPalCheckoutSdk\Orders\OrdersGetRequest;


class PaymentController extends Controller
{




    public function stripeSCAPreBuilt(){


        \Stripe\Stripe::setApiKey('sk_test_51He1vuDH9ajJHIXlmD1UdEa3ddJoGVCn6Fa8Me7JwzUzBqLF9v3QTyx9eCr127yE2WYynUrtZcTsEo91dVxQh8vE00Z2k0u7Jg');
        header('Content-Type: application/json');

        $YOUR_DOMAIN = 'http://127.0.0.1:8000';

        $checkout_session = \Stripe\Checkout\Session::create([

        'payment_method_types' => ['card'],

        'line_items' => [[

            'price_data' => [

            'currency' => 'usd',

            'unit_amount' => 2000,

                'product_data' => [

                    'name' => 'Stubborn Attachments',

                    'images' => ["https://i.imgur.com/EHyR2nP.png"],

                ],

            ],

            'quantity' => 1,

        ]],

        'mode' => 'payment',

        'success_url' => url('stripe-success'),

        'cancel_url' => url('stripe-failed'),

        ]);


        $id =  $checkout_session->id;
        session(['stripe_id' => $id]);
        return view('payment.stripe-sca-prebuilt',compact('id'));
    }


    public function stripeSCAPreBuiltCredentials(Request $request){

        \Stripe\Stripe::setApiKey('sk_test_51He1vuDH9ajJHIXlmD1UdEa3ddJoGVCn6Fa8Me7JwzUzBqLF9v3QTyx9eCr127yE2WYynUrtZcTsEo91dVxQh8vE00Z2k0u7Jg');

        header('Content-Type: application/json');

        $YOUR_DOMAIN = 'http://127.0.0.1:8000';

        $checkout_session = \Stripe\Checkout\Session::create([

        'payment_method_types' => ['card'],

        'line_items' => [[

            'price_data' => [

            'currency' => 'usd',

            'unit_amount' => 20,

                'product_data' => [

                    'name' => 'Stubborn Attachments',

                    'images' => ["https://i.imgur.com/EHyR2nP.png"],

                ],

            ],

            'quantity' => 1,

        ]],

        'mode' => 'payment',

        'success_url' => url('stripe-success'),

        'cancel_url' => url('stripe-failed'),

        ]);

        echo json_encode(['id' => $checkout_session->id]);

    }


    public function stripeSuccess(){

        return view('payment.stripe-success');
    }

    public function stripeFailed(){

        return view('payment.stripe-cancel');
    }


}
