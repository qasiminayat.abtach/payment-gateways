<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Payment;
use App\Models\Customer;
use App\Models\Transaction;
use App\Models\TransactionAttempt;
use Sample\PayPalClient;
use PayPalCheckoutSdk\Orders\OrdersGetRequest;


class PaypalController extends Controller
{

    public function paypal(){
        return view('payment.paypal.index');
    }
    public function paypalClientSide(){
        return view('payment.paypal.paypal-client-side');
    }

    public function paypalServerSide(){
        return view('payment.paypal.paypal-server-side');
    }

    public function paypalServerResponseFromClientSide(Request $request){


        $responseData = $request->data;

        $transactionArray = [
            'token'     =>      $responseData['id'],
            'amount'    =>      $responseData['purchase_units'][0]['payments']['captures'][0]['amount']['value'],
            'currency'  =>      $responseData['purchase_units'][0]['payments']['captures'][0]['amount']['currency_code'],
            'status'    =>      $responseData['purchase_units'][0]['payments']['captures'][0]['status'] == "COMPLETED" ? 'Charged' : $responseData['purchase_units'][0]['payments']['captures'][0]['status'],
            'first_4'   =>      NULL,
            'last_6'    =>      NULL,
            'fees'      =>      NULL,
        ];

        $customerDetailArray = [
            'name'          =>      $responseData['purchase_units'][0]['shipping']['name']['full_name'],
            'email'         =>      $responseData['payer']['email_address'],
            'phone'         =>      NULL,
            'country'       =>      $responseData['purchase_units'][0]['amount']['currency_code'],
            'state'         =>      NULL,
            'city'          =>      NULL,
            'zip'           =>      NULL,
            'address'       =>      $responseData['purchase_units'][0]['shipping']['address']['address_line_1'].' '.$responseData['purchase_units'][0]['shipping']['address']['admin_area_2'].' '.$responseData['purchase_units'][0]['shipping']['address']['admin_area_1'].' '.$responseData['purchase_units'][0]['shipping']['address']['postal_code'].' '.$responseData['purchase_units'][0]['shipping']['address']['country_code'],
            'merchant_id'   =>      1,
        ];



        if(!empty($responseData)){

            if($responseData['purchase_units'][0]['payments']['captures'][0]['status'] == "COMPLETED"){

                //Helper.php
                $data = storeTransaction($transactionArray, $customerDetailArray);
                $attempt = storeTransactionAttempt($transactionArray, $customerDetailArray);

                if($data == true){
                    return [
                        'status' => 'success',
                        'message' => 'Transaction status is COMPLETED',
                    ];
                }
                else{
                    return [
                        'status' => 'error',
                        'message' => 'Transaction status is COMPLETED, Error while saving payment info',
                    ];
                }

            }
            else{

                $attempt = storeTransactionAttempt($transactionArray, $customerDetailArray);

                return [
                    'status' => 'error',
                    'message' => 'Transaction status is not COMPLETED',
                ];
            }

        }
        else{

            return [
                'status' => 'error',
                'message' => 'something went wrong',
            ];
        }

    }

    public function paypalServerResponseFromServerSide(Request $request){

        $client = PayPalClient::client();
        $responseData = $client->execute(new OrdersGetRequest($request->id));

        $transactionArray = [
            'token'     =>      $responseData->result->id,
            'amount'    =>      $responseData->result->purchase_units[0]->payments->captures[0]->amount->value,
            'currency'  =>      $responseData->result->purchase_units[0]->payments->captures[0]->amount->currency_code,
            'status'    =>      $responseData->result->purchase_units[0]->payments->captures[0]->status == "COMPLETED" ? 'Charged' : $responseData->result->purchase_units[0]->payments->captures[0]->status,
            'first_4'   =>      NULL,
            'last_6'    =>      NULL,
            'fees'      =>      NULL,
        ];

        $customerDetailArray = [
            'name'          =>      $responseData->result->purchase_units[0]->shipping->name->full_name,
            'email'         =>      $responseData->result->payer->email_address,
            'phone'         =>      NULL,
            'country'       =>      $responseData->result->purchase_units[0]->amount->currency_code,
            'state'         =>      NULL,
            'city'          =>      NULL,
            'zip'           =>      $responseData->result->purchase_units[0]->shipping->address->postal_code,
            'address'       =>      $responseData->result->purchase_units[0]->shipping->address->address_line_1.' '.$responseData->result->purchase_units[0]->shipping->address->admin_area_1.' '.$responseData->result->purchase_units[0]->shipping->address->admin_area_2.' '.$responseData->result->purchase_units[0]->shipping->address->postal_code.' '.$responseData->result->purchase_units[0]->shipping->address->country_code,
            'merchant_id'   =>      1,
        ];

        if(!empty($responseData)){

            if($responseData->result->purchase_units[0]->payments->captures[0]->status == "COMPLETED"){


                //Helper.php
                $data = storeTransaction($transactionArray, $customerDetailArray);
                $attempt = storeTransactionAttempt($transactionArray, $customerDetailArray);


                if($data == true){
                    return [
                        'status' => 'success',
                        'message' => 'Transaction status is COMPLETED',
                    ];
                }
                else{
                    return [
                        'status' => 'error',
                        'message' => 'Transaction status is COMPLETED, Error while saving payment info',
                    ];
                }

            }
            else{

                $attempt = storeTransactionAttempt($transactionArray, $customerDetailArray);

                return [
                    'status' => 'error',
                    'message' => 'Transaction status is not COMPLETED',
                ];
            }

        }
        else{

            return [
                'status' => 'error',
                'message' => 'something went wrong',
            ];
        }


    }

}
