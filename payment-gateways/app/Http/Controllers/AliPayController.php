<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Pay;

class AliPayController extends Controller
{
    public function aliPay(){

        $order = [
            'out_trade_no' => time (),
            'total_amount' => '1' ,
            'subject' => 'test subject-test' ,
       ];

       return Pay::alipay()->web($order);
    }
}
