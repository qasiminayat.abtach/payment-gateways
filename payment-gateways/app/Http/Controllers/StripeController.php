<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Payment;
use App\Models\Transaction;
use App\Models\TransactionAttempt;
use App\Models\Customer;
use Slim\Http\Response;
use Stripe\Stripe;
use Session;
use Omnipay\Omnipay;
use Cartalyst\Stripe\Stripe as StripeWithoutElement;


class StripeController extends Controller
{

    public function stripe(){
        return view('payment.stripe.index');
    }
    public function stripeSCAClientSide(){
        $curl = new \Stripe\HttpClient\CurlClient([CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1]);
        $curl->setEnableHttp2(false);
        \Stripe\ApiRequestor::setHttpClient($curl);
        \Stripe\Stripe::setApiKey('sk_test_51He1vuDH9ajJHIXlmD1UdEa3ddJoGVCn6Fa8Me7JwzUzBqLF9v3QTyx9eCr127yE2WYynUrtZcTsEo91dVxQh8vE00Z2k0u7Jg');


        $intent = \Stripe\PaymentIntent::create([
        'amount' => 15000,
        'currency' => 'usd',
        // Verify your integration in this guide by including this parameter
        'metadata' => ['integration_check' => 'accept_a_payment'],
        ]);


        return view('payment.stripe.stripe-sca-client-side',compact('intent'));
    }

    public function stripeSCAServerSide(){

        $curl = new \Stripe\HttpClient\CurlClient([CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1]);
        $curl->setEnableHttp2(false);
        \Stripe\ApiRequestor::setHttpClient($curl);
        \Stripe\Stripe::setApiKey('sk_test_51He1vuDH9ajJHIXlmD1UdEa3ddJoGVCn6Fa8Me7JwzUzBqLF9v3QTyx9eCr127yE2WYynUrtZcTsEo91dVxQh8vE00Z2k0u7Jg');


        $intent = \Stripe\PaymentIntent::create([
        'amount' => 15000,
        'currency' => 'usd',
        // Verify your integration in this guide by including this parameter
        'metadata' => ['integration_check' => 'accept_a_payment'],
        ]);


        return view('payment.stripe.stripe-sca-server-side',compact('intent'));
    }


    public function stripeCPFResponseClientSide(request $request){

        $response = $request->data;

        $transactionArray = [
            'token'     =>      $response['paymentIntent']['id'],
            'amount'    =>      $response['paymentIntent']['amount']/100,
            'currency'  =>      $response['paymentIntent']['currency'],
            'status'    =>      $response['paymentIntent']['status'] == 'succeeded' ? 'Charged' : $response['paymentIntent']['status'],
            'first_4'   =>      NULL,
            'last_6'    =>      NULL,
            'fees'      =>      NULL,
        ];

        $customerDetailArray = [
            'name'          =>      NULL,
            'email'         =>      NULL,
            'phone'         =>      NULL,
            'country'       =>      NULL,
            'state'         =>      NULL,
            'city'          =>      NULL,
            'zip'           =>      NULL,
            'address'       =>      NULL,
            'merchant_id'   =>      2,
        ];

        if($response['paymentIntent']['status'] == "succeeded"){

            //Helper.php
            $data = storeTransaction($transactionArray, $customerDetailArray);
            $attempt = storeTransactionAttempt($transactionArray, $customerDetailArray);

            if($data == true){
                return [
                    'status' => 'success',
                    'message' => 'Transaction status is '.$response['paymentIntent']['status'],
                ];
            }
            else{
                return [
                    'status' => 'error',
                    'message' => 'Transaction status is COMPLETED, Error while saving payment info',
                ];
            }

        }
        else{

            $attempt = storeTransactionAttempt($transactionArray, $customerDetailArray);

            return [
                'status' => 'error',
                'message' => 'Transaction status is not COMPLETED',
            ];
        }
    }

    public function stripeCPFResponseServerSide(request $request){

        $intentId = $request->data;

        \Stripe\Stripe::setApiKey('sk_test_51He1vuDH9ajJHIXlmD1UdEa3ddJoGVCn6Fa8Me7JwzUzBqLF9v3QTyx9eCr127yE2WYynUrtZcTsEo91dVxQh8vE00Z2k0u7Jg');

        $intent = \Stripe\PaymentIntent::retrieve($intentId);


        $transactionArray = [
            'token'     =>      $intent->id,
            'amount'    =>      $intent->amount/100,
            'currency'  =>      $intent->currency,
            'status'    =>      $intent->status == "succeeded" ? 'Charged' : $intent->status,
            'first_4'   =>      NULL,
            'last_6'    =>      $intent->charges->data[0]->payment_method_details->card->last4,
            'fees'      =>      $intent->charges->data[0]->application_fee_amount,
        ];

        $customerDetailArray = [
            'name'          =>      $intent->charges->data[0]->billing_details->name,
            'email'         =>      $intent->charges->data[0]->billing_details->email,
            'phone'         =>      $intent->charges->data[0]->billing_details->phone,
            'country'       =>      $intent->charges->data[0]->billing_details->address->country,
            'state'         =>      $intent->charges->data[0]->billing_details->address->state,
            'city'          =>      $intent->charges->data[0]->billing_details->address->city,
            'zip'           =>      $intent->charges->data[0]->billing_details->address->postal_code,
            'address'       =>      $intent->charges->data[0]->billing_details->address->line1.' '.$intent->charges->data[0]->billing_details->address->state.' '.$intent->charges->data[0]->billing_details->address->city.' '.$intent->charges->data[0]->billing_details->address->country.' '.$intent->charges->data[0]->billing_details->address->postal_code,
            'merchant_id'   =>      2,
        ];

        if($intent->status == "succeeded"){


            //Helper.php
            $data = storeTransaction($transactionArray, $customerDetailArray);
            $attempt = storeTransactionAttempt($transactionArray, $customerDetailArray);

            if($data == true){
                return [
                    'status' => 'success',
                    'message' => 'Transaction status is '.$intent->status,
                ];
            }
            else{
                return [
                    'status' => 'error',
                    'message' => 'Transaction status is COMPLETED, Error while saving payment info',
                ];
            }

        }
        else{

            $attempt = storeTransactionAttempt($transactionArray, $customerDetailArray);

            return [
                'status' => 'error',
                'message' => 'Transaction status is not COMPLETED',
            ];
        }
    }


    public function stripeSCAPreBuiltCheckout(Request $request){

        return view('payment.stripe.stripe-sca-prebuilt');

    }


    public function createSession(){

        // Set API key
        \Stripe\Stripe::setApiKey('sk_test_51He1vuDH9ajJHIXlmD1UdEa3ddJoGVCn6Fa8Me7JwzUzBqLF9v3QTyx9eCr127yE2WYynUrtZcTsEo91dVxQh8vE00Z2k0u7Jg');

        $response = array(
            'status' => 0,
            'error' => array(
                'message' => 'Invalid Request!'
            )
        );


        $input = file_get_contents('php://input');
        $request = json_decode($input);


        if (json_last_error() !== JSON_ERROR_NONE) {
            http_response_code(400);
            echo json_encode($response);
            exit;
        }

        if(!empty($request->checkoutSession)){
            // Create new Checkout Session for the order
            try {
                $session = \Stripe\Checkout\Session::create([
                    'payment_method_types' => ['card'],
                    'line_items' => [[
                        'price_data' => [
                            'currency' => 'usd',
                            'unit_amount' => 500,
                                'product_data' => [
                                    'name' => 'Stubborn Attachments',
                                    'images' => ["https://i.imgur.com/EHyR2nP.png"],
                                ],
                        ],
                        'quantity' => 1,
                        'description' => 'Descrition will be here',
                    ]],
                    'mode' => 'payment',
                    'success_url' => url('stripe-success'),
                    'cancel_url' => url('stripe-failed'),

                ]);
            }catch(Exception $e) {
                $api_error = $e->getMessage();
            }

            if(empty($api_error) && $session){
                session(['stripeCheckoutSessionId' => $session->id]);
                $response = array(
                    'status' => 1,
                    'message' => 'Checkout Session created successfully!',
                    'sessionId' => $session['id']
                );
            }else{
                $response = array(
                    'status' => 0,
                    'error' => array(
                        'message' => 'Checkout Session creation failed! '.$api_error
                    )
                );
            }
        }

        // Return response
        echo json_encode($response);

    }

    public function stripeSuccess(){

        $session_id = Session::get('stripeCheckoutSessionId');

        \Stripe\Stripe::setApiKey('sk_test_51He1vuDH9ajJHIXlmD1UdEa3ddJoGVCn6Fa8Me7JwzUzBqLF9v3QTyx9eCr127yE2WYynUrtZcTsEo91dVxQh8vE00Z2k0u7Jg');

        $checkout_session = \Stripe\Checkout\Session::retrieve($session_id);
        $intent = \Stripe\PaymentIntent::retrieve($checkout_session->payment_intent);


        $transactionArray = [
            'token'     =>      $intent->id,
            'amount'    =>      $intent->amount/100,
            'currency'  =>      $intent->currency,
            'status'    =>      $intent->status == "succeeded" ? 'Charged' : $intent->status,
            'first_4'   =>      NULL,
            'last_6'    =>      $intent->charges->data[0]->payment_method_details->card->last4,
            'fees'      =>      $intent->charges->data[0]->application_fee_amount,
        ];

        $customerDetailArray = [
            'name'          =>      $intent->charges->data[0]->billing_details->name,
            'email'         =>      $intent->charges->data[0]->billing_details->email,
            'phone'         =>      $intent->charges->data[0]->billing_details->phone,
            'country'       =>      $intent->charges->data[0]->billing_details->address->country,
            'state'         =>      $intent->charges->data[0]->billing_details->address->state,
            'city'          =>      $intent->charges->data[0]->billing_details->address->city,
            'zip'           =>      $intent->charges->data[0]->billing_details->address->postal_code,
            'address'       =>      $intent->charges->data[0]->billing_details->address->line1.' '.$intent->charges->data[0]->billing_details->address->state.' '.$intent->charges->data[0]->billing_details->address->city.' '.$intent->charges->data[0]->billing_details->address->country.' '.$intent->charges->data[0]->billing_details->address->postal_code,
            'merchant_id'   =>      2,
        ];


        if(!empty($checkout_session)){

            if(!empty($intent)){
                if($intent->status == 'succeeded'){

                    //Helper.php
                    $data = storeTransaction($transactionArray, $customerDetailArray);
                    $attempt = storeTransactionAttempt($transactionArray, $customerDetailArray);

                    Session::forget('stripeCheckoutSessionId');

                    return view('payment.stripe.stripe-success');
                }
            }
            else{

                dd('error');
            }
        }
        else{
            dd('error');
        }


    }

    public function stripeFailed(){


        $session_id = Session::get('stripeCheckoutSessionId');

        \Stripe\Stripe::setApiKey('sk_test_51He1vuDH9ajJHIXlmD1UdEa3ddJoGVCn6Fa8Me7JwzUzBqLF9v3QTyx9eCr127yE2WYynUrtZcTsEo91dVxQh8vE00Z2k0u7Jg');

        $checkout_session = \Stripe\Checkout\Session::retrieve($session_id);
        $intent = \Stripe\PaymentIntent::retrieve($checkout_session->payment_intent);


        $transactionArray = [
            'token'     =>      $intent->id,
            'amount'    =>      $intent->amount/100,
            'currency'  =>      $intent->currency,
            'status'    =>      $intent->status,
            'first_4'   =>      NULL,
            'last_6'    =>      $intent->charges->data[0]->payment_method_details->card->last4,
            'fees'      =>      $intent->charges->data[0]->application_fee_amount,
        ];

        $customerDetailArray = [
            'name'          =>      $intent->charges->data[0]->billing_details->name,
            'email'         =>      $intent->charges->data[0]->billing_details->email,
            'phone'         =>      $intent->charges->data[0]->billing_details->phone,
            'country'       =>      $intent->charges->data[0]->billing_details->address->country,
            'state'         =>      $intent->charges->data[0]->billing_details->address->state,
            'city'          =>      $intent->charges->data[0]->billing_details->address->city,
            'zip'           =>      $intent->charges->data[0]->billing_details->address->postal_code,
            'address'       =>      $intent->charges->data[0]->billing_details->address->line1.' '.$intent->charges->data[0]->billing_details->address->state.' '.$intent->charges->data[0]->billing_details->address->city.' '.$intent->charges->data[0]->billing_details->address->country.' '.$intent->charges->data[0]->billing_details->address->postal_code,
            'merchant_id'   =>      2,
        ];

        //Helper.php
        $attempt = storeTransactionAttempt($transactionArray, $customerDetailArray);

        Session::forget('stripeCheckoutSessionId');

        return view('payment.stripe.stripe-cancel');


    }


    public function stripeWithoutElement(){
        return view('payment.stripe.stripe-without-element');
    }


    public function stripeWithoutElementSave(Request $request){

        $stripe = StripeWithoutElement::make('sk_test_4Osrs9nRE3mXfEsMLA6NNo1100VNG1OxLZ');


        $cardNumber = str_replace("-", "", $request->cardNumber);
        $cardExpiry = str_replace("/", "", $request->cardExpiry);

        $first4 = substr($cardNumber, 0,4);
        $last6 = substr($cardNumber, -6);


        $credit_card = [
            'number' => $cardNumber,
            'name' => 'Qasim Inayat',
            'exp_month' => '9',
            'exp_year' => '2030',
            'cvc' => '123',
            'address_country' => 'Pakistan',
            'address_state' => 'Sindh',
            'address_city' => 'Karachi',
            'address_zip'=> '74700',
            'address_line1' => 'North Nazimbad, Karachi',

        ];





        $token = $stripe->tokens()->create([
            'card' => $credit_card,
        ]);

        $email = 'qasiminayat93@gmail.com';

        $customer = $stripe->customers()->create(array(
            "description" => "Customer Id " . $email,
            "source" => $token['id'],
        ));



        $charge_data = [
            'amount' => '12',
            'currency' => 'USD',
            'description' => 'Customer ID',
            'capture' => true,
            'statement_descriptor' => "DesignIconic",
            'customer' => $customer['id'],
            'metadata' => [
                'receipt_email' => $email,
                'customer_name' => $email,
                'brand' => "Design Iconic US",
                'services' => 'DesignIconic',
                'package' => 'GOLD',
                'agent' => '23',
            ],
        ];


        $charge_object = $stripe->charges()->create($charge_data);

        $transactionArray = [
            'token'     =>      $charge_object['id'],
            'amount'    =>      $charge_object['amount'],
            'currency'  =>      $charge_object['currency'],
            'status'    =>      $charge_object['status'],
            'first_4'   =>      $first4,
            'last_6'    =>      $last6,
            'fees'      =>      NULL,
        ];

        $customerDetailArray = [
            'name'          =>      $charge_object['billing_details']['name'],
            'email'         =>      $charge_object['billing_details']['email'],
            'phone'         =>      $charge_object['billing_details']['phone'],
            'country'       =>      $charge_object['billing_details']['address']['country'],
            'state'         =>      $charge_object['billing_details']['address']['state'],
            'city'          =>      $charge_object['billing_details']['address']['city'],
            'zip'           =>      $charge_object['billing_details']['address']['city'],
            'address'       =>      $charge_object['billing_details']['address']['line1'],
            'merchant_id'   =>      2,
        ];




        if($charge_object['status'] == "succeeded"){


            //Helper.php
            $data = storeTransaction($transactionArray, $customerDetailArray);
            $attempt = storeTransactionAttempt($transactionArray, $customerDetailArray);

            if($data == true){
                return redirect()->back()->with('success', 'Transaction succeeded');
            }
            else{

                return redirect()->back()->with('error', 'Transaction status is COMPLETED, Error while saving payment info');
            }

        }
        else{


            $attempt = storeTransactionAttempt($transactionArray, $customerDetailArray);

            return redirect()->back()->with('error', 'Transaction failed');
        }



    }



}
