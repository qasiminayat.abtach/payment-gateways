<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Payeezy\Payeezy_Client;
use App\Payeezy\Payeezy_CreditCard;
use App\Payeezy\Payeezy_TransactionType;
use App\Payeezy\Payeezy_Error;
use App\Models\Payment;
use App\Models\Transaction;
use App\Models\Customer;
use App\Models\TransactionAttempt;





class PayeezyController extends Controller
{
    public function payeezy(){

        return view('payment.payeezy.index');

    }

    public function payeezyIntegration(){
        return view('payment.payeezy.payeezy');
    }

    public function payeezyIntegrationResponse(Request $request){

        $client = new Payeezy_Client();
        $client->setApiKey("RiOiYMxhlxQ2CnNqRQGr4eNpZmY8RTeY");
        $client->setApiSecret("94fda77c14af4af3d0b7acc5d02773c9759ee267792b42568c2583abfe02bc1d");
        $client->setMerchantToken("fdoa-4a2f5eb6918f92d0763d674a6d45fb15130110239162d0b6");
        $client->setUrl("https://api-cert.payeezy.com/v1/transactions");



        $card_transaction = new Payeezy_CreditCard($client);

        $cardNumber = str_replace("-", "", $request->cardNumber);
        $cardExpiry = str_replace("/", "", $request->cardExpiry);

        $first4 = substr($cardNumber, 0,4);
        $last6 = substr($cardNumber, -6);


        $response = $card_transaction->purchase([
            "merchant_ref" => "Astonishing-Sale",
            "amount" => "500",
            "currency_code" => "USD",
            "credit_card" => array(
                "type" => "visa",
                "cardholder_name" => "Qasim Inayat",
                "card_number" => $cardNumber,
                "exp_date" => $cardExpiry,
                "cvv" => $request->cardCVC
            ),
        ]);


        $array1 =  [];
        $array2  = [];
        $count = 0;

        if(isset($response->messages)){

            foreach($response->messages as $index=>$message){
                foreach($response->messages[$index] as $index2=>$m){
                    $array1[$index2] = $message;
                }
            }
            foreach($array1 as $index3=>$q){
                $array2[$count] = $q;
                $count++;
            }

            $errorMessage = $array2[0];

            return redirect()->back()->with('error', $errorMessage);
        }
        else{

            $transactionArray = [
                'token'     =>      $response->transaction_id,
                'amount'    =>      $response->amount,
                'currency'  =>      $response->currency,
                'status'    =>      $response->transaction_status == 'approved'  ? 'Charged' : $response->transaction_status,
                'first_4'   =>      $first4,
                'last_6'    =>      $last6,
                'fees'      =>      NULL,
            ];


            $customerDetailArray = [
                'name'          =>      'Qasim Inayat',
                'email'         =>      'qasiminayat93@gmail.com',
                'phone'         =>       03002742731,
                'country'       =>      'Pakistan',
                'state'         =>      'Sindh',
                'city'          =>      'Karachi',
                'zip'           =>      74700,
                'address'       =>      'North Nazimabad, Karachi',
                'merchant_id'   =>      3,
            ];



            if($response->transaction_status == 'approved'){

                //Helper.php
                $data = storeTransaction($transactionArray, $customerDetailArray);
                $attempt = storeTransactionAttempt($transactionArray, $customerDetailArray);

                return redirect()->back()->with('success', 'Trasaction succeeded');

            }
            else{

                //Helper.php
                $attempt = storeTransactionAttempt($transactionArray, $customerDetailArray);
                return redirect()->back()->with('errorr', 'Transaction failed');

            }
        }
    }
}
