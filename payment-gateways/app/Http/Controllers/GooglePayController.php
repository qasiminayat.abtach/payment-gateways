<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TransactionAttempt;

class GooglePayController extends Controller
{
    public function googlePay(){
        return view('payment.google.google');
    }
}
