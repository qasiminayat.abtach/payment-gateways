<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;
use App\Models\Payment;
use App\Models\Transaction;
use App\Models\TransactionAttempt;
use App\Models\Customer;

class AuthorizeDotNetConroller extends Controller
{
    public function index(){
        return view('payment.authorize.index');
    }

    public function authorizeDotNet(){
        return view('payment.authorize.authorize-dot-net');
    }

    public function authorizeDotNetResponse(Request $r){

        $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
        $merchantAuthentication->setName('45Qk4cuJ5Jq7');
        $merchantAuthentication->setTransactionKey('663gJCkUdL58344q');

        $refId = 'ref'.time();
        // 4111111111111111


        $cardNumber = str_replace("-", "", $r->cardNumber);
        $first4 = substr($cardNumber, 0,4);
        $last6 = substr($cardNumber, -6);

        $creditCard = new AnetAPI\CreditCardType();
        $creditCard->setCardNumber($cardNumber);
        $creditCard->setCardCode($r->cardCVC);
        $creditCard->setExpirationDate($r->cardExpiry);

        $paymentOne = new AnetAPI\PaymentType();
        $paymentOne->setCreditCard($creditCard);


        $customerAddress = new AnetAPI\CustomerAddressType();
        $customerAddress->setPhoneNumber("03002742731");
        $customerAddress->setEmail("qasiminayat93@gmail.com");
        $customerAddress->setFirstName("Qasim");
        $customerAddress->setLastName("Inayat");
        $customerAddress->setCompany("Genuine Trade");
        $customerAddress->setAddress("North Nazimbad, Karachi");
        $customerAddress->setCity("Karachi");
        $customerAddress->setState("State");
        $customerAddress->setZip("74700");
        $customerAddress->setCountry("Pakistan");

        $transactionRequestType = new AnetAPI\TransactionRequestType();
        $transactionRequestType->setTransactionType('authCaptureTransaction');
        $transactionRequestType->setAmount('500');
        $transactionRequestType->setCurrencyCode('USD');
        $transactionRequestType->setPayment($paymentOne);
        $transactionRequestType->setBillTo($customerAddress);

        $request = new AnetAPI\CreateTransactionRequest();
        $request->setMerchantAuthentication($merchantAuthentication);
        $request->setRefId($refId);
        $request->setTransactionRequest($transactionRequestType);

        $setTransactionDetails = new AnetController\CreateTransactionController($request);

        $getDetails = collect($transactionRequestType);
        $getBillingDetails = collect($getDetails['billTo']);

        $response = $setTransactionDetails->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);

        $authorizeResponse = "";


        $transactionArray = [
            'token'     =>      $refId,
            'amount'    =>      $getDetails['amount'],
            'currency'  =>      $getDetails['currencyCode'],
            'status'    =>      $response->getMessages()->getResultCode() == "Ok"  ? 'Charged' :  $response->getMessages()->getResultCode(),
            'first_4'   =>      $first4,
            'last_6'    =>      $last6,
            'fees'      =>      NULL,
        ];

        $customerDetailArray = [
            'name'          =>      $getBillingDetails['firstName'].' '.$getBillingDetails['lastName'],
            'email'         =>      $getBillingDetails['email'],
            'phone'         =>      $getBillingDetails['phoneNumber'],
            'country'       =>      $getBillingDetails['country'],
            'state'         =>      $getBillingDetails['state'],
            'city'          =>      $getBillingDetails['city'],
            'zip'           =>      $getBillingDetails['zip'],
            'address'       =>      $getBillingDetails['address'].' '.$getBillingDetails['city'].' '.$getBillingDetails['state'].' '.$getBillingDetails['country'].' '.$getBillingDetails['zip'],
            'merchant_id'   =>      4,
        ];

        if ($response != null){
            if($response->getMessages()->getResultCode() == "Ok"){
                $tresponse = $response->getTransactionResponse();

                if($tresponse != null && $tresponse->getMessages() != null){


                    //Helper.php
                    $data = storeTransaction($transactionArray, $customerDetailArray);
                    $attempt = storeTransactionAttempt($transactionArray, $customerDetailArray);

                    $authorizeResponse.=" Transaction Response code : " . $tresponse->getResponseCode(). "\n";
                    $authorizeResponse.=" AUTH CODE : " . $tresponse->getAuthCode(). "\n";
                    $authorizeResponse.=" TRANS ID  : " . $tresponse->getTransId(). "\n";
                    $authorizeResponse.=" Code : " . $tresponse->getMessages()[0]->getCode(). "\n";
                    $authorizeResponse.=" Description : " . $tresponse->getMessages()[0]->getDescription(). "\n";

                }
                else{



                    $attempt = storeTransactionAttempt($transactionArray, $customerDetailArray);

                    $authorizeResponse.="Transaction Failed". "\n";
                    if($tresponse->getErrors() != null)
                    {
                        $authorizeResponse.=" Error code  : " . $tresponse->getErrors()[0]->getErrorCode(). "\n";
                        $authorizeResponse.=" Error message : " . $tresponse->getErrors()[0]->getErrorText(). "\n";
                    }
                }
            }
            else{

                $attempt = storeTransactionAttempt($transactionArray, $customerDetailArray);


                $authorizeResponse.="Transaction Failed". "\n";
                $tresponse = $response->getTransactionResponse();
                if($tresponse != null && $tresponse->getErrors() != null){
                    $authorizeResponse.=" Error code  : " . $tresponse->getErrors()[0]->getErrorCode(). "\n";
                    $authorizeResponse.=" Error message : " . $tresponse->getErrors()[0]->getErrorText(). "\n";
                }
                else{
                    $authorizeResponse.=" Error code  : " . $response->getMessages()->getMessage()[0]->getCode(). "\n";
                    $authorizeResponse.=" Error message : " . $response->getMessages()->getMessage()[0]->getText(). "\n";
                }
            }
        }
        else{
             "No response returned \n";
        }

       if($response->getMessages()->getResultCode() == "Ok"){
            return redirect()->back()->with('success',$authorizeResponse);
       }
       else{
            return redirect()->back()->with('error',$authorizeResponse);
       }

    }
}
