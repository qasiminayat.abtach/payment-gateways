<!DOCTYPE html>
<html lang="en">
<head>
  <title>Payment Integrations</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
   @include('partials.styles')
   @stack('styles')
</head>
<body>

@include('partials.header')



<div class="container text-center" style="margin-top:150px;">
    @yield('content')
</div><br>

    @include('partials.scripts')
    @stack('scripts')
</body>
</html>
