<!DOCTYPE html>

<html>

  <head>

    <title>Buy cool new product</title>

    <link rel="stylesheet" href="style.css">

    <script src="https://polyfill.io/v3/polyfill.min.js?version=3.52.1&features=fetch"></script>

    <script src="https://js.stripe.com/v3/"></script>

  </head>

  <body>

    <section>

      <div class="product">

        <img

          src="https://i.imgur.com/EHyR2nP.png"

          alt="The cover of Stubborn Attachments"

        />

        <div class="description">

          <h3>Stubborn Attachments</h3>

          <h5>$20.00</h5>

        </div>

      </div>

      <button id="checkout-button">Checkout</button>

    </section>

  </body>

  <script type="text/javascript">

    // Create an instance of the Stripe object with your publishable API key

    var stripe = Stripe("pk_test_51He1vuDH9ajJHIXlDcvyzzNLrxNkWxpy0zEWMUL6GkOZnncGye7O3iLlQvNsnc3jgwoBSVfsEdmlyGhLM66Uzggb00Cc4YXsyI");


    stripe.redirectToCheckout({
        sessionId : '{{$id}}'
    }).thne(function(result){

    });

  </script>

</html>
