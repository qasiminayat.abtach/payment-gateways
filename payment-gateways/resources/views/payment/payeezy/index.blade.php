@extends('layout.scaffold')
@section('content')
<div class="row">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-body">
          <img src="https://images.squarespace-cdn.com/content/v1/57a787bd6a4963e38670cb53/1477346999238-KY1V1DJCQJ3DXOCBJ63S/ke17ZwdGBToddI8pDm48kHJRCKtOdK6rQ_s748O8KQ3lfiSMXz2YNBs8ylwAJx2qrCLSIWAQvdC7iWmC9HNtRduo4g5L5DKnUsLAtx31bFfhNoAsh75UU5au9daFUUS0LgWLOZNN8y_3cedSwIJXaQ/payeezy+logo.png" width="250px;">
          <h3 class="card-title">Pyeezy</h3>
          <a href="{{url('payeezy-integration')}}" class="btn btn-primary">Test it</a>
        </div>
      </div>
    </div>
</div>
@endsection
