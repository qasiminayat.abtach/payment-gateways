@extends('layout.scaffold')
@section('content')
<div class="row">
    <div class="col-sm-6">
      <div class="card">
        <div class="card-body">
          <img src="https://www.websouls.com/public/uploads/images/cms/block/240x130/paypal.png">
          <h3 class="card-title">Client Side</h3>
          <a href="{{url('paypal-client-side')}}" class="btn btn-primary">Test it</a>
        </div>
      </div>
    </div>
    <div class="col-sm-6">
        <div class="card">
          <div class="card-body">
            <img src="https://www.websouls.com/public/uploads/images/cms/block/240x130/paypal.png">
        <h3 class="card-title">Server Side</h3>
            <a href="{{url('/paypal-server-side')}}" class="btn btn-primary">Test it</a>
          </div>
        </div>
      </div>
</div>
@endsection
