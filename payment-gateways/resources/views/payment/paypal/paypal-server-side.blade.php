@extends('layout.scaffold')
@section('content')
<div class="row">
    <div class="col-md-12" style="margin-bottom:5px;">
        <button class="btn btn-success" data-toggle="modal" data-target="#paypalCredentials">Show Credentials</button>
    </div>
</div>
<div id="paypal-button-container"></div>
@include('partials.modals')
@endsection
@push('scripts')
<script src="https://www.paypal.com/sdk/js?client-id=AfJM_9c45vcNtISwCwtLDAc9QtahdvtmF0_MVLeIkqXZR7lOITReAt0MedKwnLWIUSqagUPYnPE4hEwF&currency=USD"></script>
<script>
    // Render the PayPal button into #paypal-button-container
    paypal.Buttons({

        // Call your server to set up the transaction
        createOrder: function(data, actions) {
            return actions.order.create({
                purchase_units: [{
                    amount: {
                        value: '0.01'
                    }
                }]
            });
        },

        // Finalize the transaction
        onApprove: function(data, actions) {
            return actions.order.capture().then(function(details) {
                // Show a success message to the buyer
                // window.location = "{{url('store-files/paypal-transaction-complete')}}"+"/"+data.orderID;
                $.ajax({
                    type : "POST",
                    url  : "{{url('paypal-server-response-from-server-side')}}",
                    data : {
                        "_token" : "{{csrf_token()}}",
                        id       :  data.orderID,
                    },
                    success:function(res){
                        if(res.status=="success"){
                            alert(res.message);
                            setTimeout(function() {
                                location.reload();
                            }, 1000);
                        }
                        else if(res.status=="error"){
                            alert(res.message);
                        }
                        else{
                            alert('Something went wrong');
                        }
                    },
                    error:function(res){
                        alert('Something went wrong');
                    }
                })
            });
        }


    }).render('#paypal-button-container');
</script>
@endpush
