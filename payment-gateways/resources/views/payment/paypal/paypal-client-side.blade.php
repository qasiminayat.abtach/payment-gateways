@extends('layout.scaffold')
@section('content')
<div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4" style="margin-bottom:8px;">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Paypal Client Side Payment</h4>
                <img  src="https://i.imgur.com/EHyR2nP.png"  style="width:100%">
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                <p>Price: <b>0.01 USD</b></p>
                <button class="btn btn-success" style="margin-bottom:5px; margin-top:5px;" data-toggle="modal" data-target="#paypalCredentials">Show Credentials</button>
                <div id="paypal-button-container"></div>
            </div>
        </div>
    </div>
    <div class="col-md-4"></div>
</div>

@include('partials.modals')
@endsection
@push('scripts')
<script src="https://www.paypal.com/sdk/js?client-id=AfJM_9c45vcNtISwCwtLDAc9QtahdvtmF0_MVLeIkqXZR7lOITReAt0MedKwnLWIUSqagUPYnPE4hEwF&currency=USD"></script>
<script>
    // Render the PayPal button into #paypal-button-container
    paypal.Buttons({

        // Set up the transaction
        createOrder: function(data, actions) {
            return actions.order.create({
                purchase_units: [{
                    amount: {
                        value: '0.01'
                    }
                }]
            });
        },

        // Finalize the transaction
        onApprove: function(data, actions) {
            return actions.order.capture().then(function(details) {
                // Show a success message to the buyer
                $.ajax({
                    type : "POST",
                    url  : "{{url('paypal-server-response-from-client-side')}}",
                    data : {
                        "_token" : "{{csrf_token()}}",
                        data       :  details,
                    },
                    success:function(res){
                        if(res.status=="success"){
                            alert(res.message);
                            setTimeout(function() {
                                location.reload();
                            }, 1000);
                        }
                        else if(res.status=="error"){
                            alert(res.message);
                        }
                        else{
                            alert('Something went wrong');
                        }
                    },
                    error:function(res){
                        alert('Something went wrong');
                    }
                })
                // alert('Transaction completed by ' + details.payer.name.given_name + '!');
            });
        }


    }).render('#paypal-button-container');
</script>
@endpush
