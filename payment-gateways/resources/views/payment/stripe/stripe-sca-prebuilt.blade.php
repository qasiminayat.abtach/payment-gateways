@extends('layout.scaffold')
@push('styles')
<style>
    .StripeElement {
        box-sizing: border-box;

        height: 40px;

        padding: 10px 12px;

        border: 1px solid #ddd;
        border-radius: 4px;
        background-color: white;

        box-shadow: 0 1px 3px 0 #e6ebf1;
        -webkit-transition: box-shadow 150ms ease;
        transition: box-shadow 150ms ease;
    }

    .StripeElement--focus {
        box-shadow: 0 1px 3px 0 #cfd7df;
    }

    .StripeElement--invalid {
        border-color: #fa755a;
    }

    .StripeElement--webkit-autofill {
        background-color: #fefde5 !important;
    }
    .loader{ position: fixed; left: 0px; top: 0px; width: 100%; height: 100%; z-index: 9999; background: url('http://localhost/laravel-test/public/images/loader/loader.gif') 50% 50% no-repeat rgb(255,255,255,0.5); background-size: 120px; }
</style>

@endpush
@section('content')
<div class="row">
    <div class="col-md-12" style="margin-bottom:5px;">
        <button class="btn btn-success" data-toggle="modal" data-target="#stripeCredentials">Show Credentials</button>
    </div>
</div>
<div id="paymentResponse"></div>
<div class="container mt-5">
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Stripe Pre Built</h4>
                    <img  src="https://i.imgur.com/EHyR2nP.png"  style="width:100%">
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                    <p>Price: <b>500 USD</b></p>
                    <button class="stripe-button btn btn-success btn-block" id="payButton">Buy Now</button>
                </div>
            </div>
        </div>
        <div class="col-md-4"></div>
    </div>
</div>
@include('partials.modals')
@endsection
@push('scripts')
<script src="https://polyfill.io/v3/polyfill.min.js?version=3.52.1&features=fetch"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://js.stripe.com/v3/"></script>
<script>
    var buyBtn = document.getElementById('payButton');
    var responseContainer = document.getElementById('paymentResponse');

    // Create a Checkout Session with the selected product
    var createCheckoutSession = function (stripe) {
        return fetch("create-session", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                checkoutSession: 1,
            }),
        }).then(function (result) {
            return result.json();
        });
    };

    // Handle any errors returned from Checkout
    var handleResult = function (result) {
        if (result.error) {
            responseContainer.innerHTML = '<p>'+result.error.message+'</p>';
        }
        buyBtn.disabled = false;
        buyBtn.textContent = 'Buy Now';
    };

    // Specify Stripe publishable key to initialize Stripe.js
    var stripe = Stripe('pk_test_51He1vuDH9ajJHIXlDcvyzzNLrxNkWxpy0zEWMUL6GkOZnncGye7O3iLlQvNsnc3jgwoBSVfsEdmlyGhLM66Uzggb00Cc4YXsyI');

    buyBtn.addEventListener("click", function (evt) {
        buyBtn.disabled = true;
        buyBtn.textContent = 'Please wait...';

        createCheckoutSession().then(function (data) {
            if(data.sessionId){
                stripe.redirectToCheckout({
                    sessionId: data.sessionId,
                }).then(handleResult);
            }else{
                handleResult(data);
            }
        });
    });
</script>
@endpush
