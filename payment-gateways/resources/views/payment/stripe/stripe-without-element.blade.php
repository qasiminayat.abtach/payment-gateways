@extends('layout.scaffold')
@push('styles')
<style>
    .StripeElement {
        box-sizing: border-box;

        height: 40px;

        padding: 10px 12px;

        border: 1px solid #ddd;
        border-radius: 4px;
        background-color: white;

        box-shadow: 0 1px 3px 0 #e6ebf1;
        -webkit-transition: box-shadow 150ms ease;
        transition: box-shadow 150ms ease;
    }

    .StripeElement--focus {
        box-shadow: 0 1px 3px 0 #cfd7df;
    }

    .StripeElement--invalid {
        border-color: #fa755a;
    }

    .StripeElement--webkit-autofill {
        background-color: #fefde5 !important;
    }
    .loader{ position: fixed; left: 0px; top: 0px; width: 100%; height: 100%; z-index: 9999; background: url('http://localhost/laravel-test/public/images/loader/loader.gif') 50% 50% no-repeat rgb(255,255,255,0.5); background-size: 120px; }
</style>

@endpush
@section('content')

@if(Session::has('error'))
<div class="alert alert-danger" role="alert">
    {{Session::get('error')}}
</div>
@endif

@if(Session::has('success'))
<div class="alert alert-success" role="alert">
    {{Session::get('success')}}
</div>
@endif
<div class="row">
    <div class="col-md-12" style="margin-bottom:5px;">
        <button class="btn btn-success" data-toggle="modal" data-target="#authorizeDotNetCredentials">Show Credentials</button>
    </div>
</div>
<div id="paymentResponse"></div>
<div class="container mt-5">
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Authorize.Net</h4>
                    <img  src="https://i.imgur.com/EHyR2nP.png"  style="width:100%">
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                    <p>Price: <b>500 USD</b></p>
                    <button class="stripe-button btn btn-success btn-block"  data-toggle="modal" data-target="#stripeWithoutElement">Buy Now</button>
                </div>
            </div>
        </div>
        <div class="col-md-4"></div>
    </div>
</div>
@include('partials.modals')
@endsection
@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js" integrity="sha512-d4KkQohk+HswGs6A1d6Gak6Bb9rMWtxjOa0IiY49Q3TeFd5xAzjWXDCBW9RS7m86FQ4RzM2BdHmdJnnKRYknxw==" crossorigin="anonymous"></script>
<script>

       $(function() {
        $(".cardNumber").mask("9999-9999-9999-9999");
        $(".cardExpiry").mask("99-99");
        $(".cardCVC").mask("9999");
    });
</script>
@endpush
