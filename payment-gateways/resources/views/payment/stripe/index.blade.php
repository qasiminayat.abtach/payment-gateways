@extends('layout.scaffold')
@section('content')
<div class="row">
    <div class="col-sm-6">
      <div class="card">
        <div class="card-body">
          <img src="https://financialit.net/sites/default/files/1200px-stripe_logo_revised_2016.svg_.png" width="250px;">
          <h3 class="card-title">Client Side</h3>
          <a href="{{url('stripe-sca-customer-payment-flow-client-side')}}" class="btn btn-primary">Test it</a>
        </div>
      </div>
    </div>
    <div class="col-sm-6">
        <div class="card">
          <div class="card-body">
            <img src="https://financialit.net/sites/default/files/1200px-stripe_logo_revised_2016.svg_.png" width="250px;">
        <h3 class="card-title">Server Side</h3>
            <a href="{{url('stripe-sca-customer-payment-flow-server-side')}}" class="btn btn-primary">Test it</a>
          </div>
        </div>
    </div>
    <div class="col-sm-6" style="margin-bottom:5px; margin-top:5px;">
        <div class="card">
          <div class="card-body">
            <img src="https://financialit.net/sites/default/files/1200px-stripe_logo_revised_2016.svg_.png" width="250px;">
        <h3 class="card-title">Stripe Without Element</h3>
            <a href="{{url('stripe-without-element')}}" class="btn btn-primary">Test it</a>
          </div>
        </div>
    </div>
    <div class="col-sm-6" style="margin-bottom:5px; margin-top:5px;">
        <div class="card">
          <div class="card-body">
            <img src="https://financialit.net/sites/default/files/1200px-stripe_logo_revised_2016.svg_.png" width="250px;">
        <h3 class="card-title">Pre Built Checkout</h3>
            <a href="{{url('stripe-sca-pre-built-checkout')}}" class="btn btn-primary">Test it</a>
          </div>
        </div>
    </div>
</div>
@endsection
