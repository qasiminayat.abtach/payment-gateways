@extends('layout.scaffold')
@push('styles')
<style>
    .StripeElement {
        box-sizing: border-box;

        height: 40px;

        padding: 10px 12px;

        border: 1px solid #ddd;
        border-radius: 4px;
        background-color: white;

        box-shadow: 0 1px 3px 0 #e6ebf1;
        -webkit-transition: box-shadow 150ms ease;
        transition: box-shadow 150ms ease;
    }

    .StripeElement--focus {
        box-shadow: 0 1px 3px 0 #cfd7df;
    }

    .StripeElement--invalid {
        border-color: #fa755a;
    }

    .StripeElement--webkit-autofill {
        background-color: #fefde5 !important;
    }
    .loader{ position: fixed; left: 0px; top: 0px; width: 100%; height: 100%; z-index: 9999; background: url('http://localhost/laravel-test/public/images/loader/loader.gif') 50% 50% no-repeat rgb(255,255,255,0.5); background-size: 120px; }
</style>

@endpush
@section('content')
<div class="loader" style="display:none;"></div>
<div class="container mt-5">
    <div class="row">
        <div class="col-md-12" style="margin-bottom:5px;">
            <button class="btn btn-success" data-toggle="modal" data-target="#stripeCredentials">Show Credentials</button>
        </div>
    </div>
	<div class="row">
        <div class="col-md-4"></div>
		<div class="col-md-4 ">
			<div class="card">
				<div class="card-body">
					<h4 class="card-title">Stripe Payment</h4>
					<form id="payment-form">
						<div class="form-group">
							<label class="font-weight-bold">Full Name</label>
							<input type="text" id="fullname" name="fullname" class="form-control" placeholder="Full Name">
						</div>
                       <div class="form-group">
                       	    <label class="font-weight-bold">Card Details</label>
                            <div id="card-element"></div>
                            <!-- We'll put the error messages in this element -->
                            <div id="card-errors" role="alert"></div>
                        </div>

                        <div class="form-group">
                        	<button id="submit" class="btn btn-block btn-success">Pay Now</button>
                        </div>
					</form>
				</div>
			</div>
        </div>
        <div class="col-md-4"></div>
	</div>
</div>
@include('partials.modals')
@endsection
@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://js.stripe.com/v3/"></script>
<script type="text/javascript">

    var stripe = Stripe('pk_test_51He1vuDH9ajJHIXlDcvyzzNLrxNkWxpy0zEWMUL6GkOZnncGye7O3iLlQvNsnc3jgwoBSVfsEdmlyGhLM66Uzggb00Cc4YXsyI');
    var elements = stripe.elements();

    // Set up Stripe.js and Elements to use in checkout form
    var style = {
      base: {
        color: "#32325d",
      }
    };

    var card = elements.create("card", { style: style });
    card.mount("#card-element");


    card.addEventListener('change', ({error}) => {
      const displayError = document.getElementById('card-errors');
      if (error) {
        displayError.textContent = error.message;
      } else {
        displayError.textContent = '';
      }
    });

    var form = document.getElementById('payment-form');

    form.addEventListener('submit', function(ev) {
      ev.preventDefault();
      stripe.confirmCardPayment('{{$intent->client_secret}}', {
        payment_method: {
          card: card,
          billing_details: {
            address: {
                city: null,
                country: "US",
                line1: "North Naizmbad, karachi, paksitan",
                postal_code: 74700,
                state: null
            },
            name: $("#fullname").val(),
            email: "qasiminayat93@gmai.com",
            phone: "03002742731"
          }
        }
      }).then(function(result) {
        if (result.error) {
          // Show error to your customer (e.g., insufficient funds)

          $('#card-errors').text(result.error.message);
        } else {
          // The payment has been processed!
          if (result.paymentIntent.status === 'succeeded') {
            // Show a success message to your customer
            // There's a risk of the customer closing the window before callback
            // execution. Set up a webhook or plugin to listen for the
            // payment_intent.succeeded event that handles any business critical
            // post-payment actions.

               $.ajax({
                   type : "POST",
                   url  : "{{url('stripe-cpf-response-client-side')}}",
                   data : {
                       "_token" : "{{csrf_token()}}",
                       data : result,
                   },
                   beforeSend:function(){
                     $(".loader").show();
                   },
                   success:function(res){
                    $(".loader").hide();
                    if(res.status=="success"){
                        alert(res.message);
                    }
                    else if(res.status=="error"){
                        alert(res.message);
                    }
                    else{
                        alert('Something went wrong');
                    }

                   },
                   error:function(res){
                    $(".loader").hide();
                    alert('Something went wrong');
                   }
               })


            $('#card-errors').text('Payment Completed');

          }
        }
      });
    });
</script>
@endpush
