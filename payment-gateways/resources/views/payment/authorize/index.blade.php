@extends('layout.scaffold')
@section('content')
<div class="row">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-body">
          <img src="https://financesonline.com/uploads/2020/06/Authorize.Net-logo1.png" width="250px;">
          <h3 class="card-title">Authorize.Net</h3>
          <a href="{{url('authorize-dot-net')}}" class="btn btn-primary">Test it</a>
        </div>
      </div>
    </div>
</div>
@endsection
