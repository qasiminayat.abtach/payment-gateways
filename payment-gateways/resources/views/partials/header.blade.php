<nav class="navbar navbar-inverse">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>

      </div>
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav">
          <li><a href="{{url('paypal')}}">Paypal</a></li>
          <li><a href="{{url('stripe')}}">Stripe</a></li>
          <li><a href="{{url('payeezy')}}">Payeezy</a></li>
          <li><a href="{{url('authorize')}}">Authorize.net</a></li>
          <li><a href="{{url('google-pay')}}">Google Pay</a></li>
          <li><a href="#">Chase Orbital</a></li>
          <li><a href="#">Apple Wallet</a></li>

        </ul>

      </div>
    </div>
</nav>
