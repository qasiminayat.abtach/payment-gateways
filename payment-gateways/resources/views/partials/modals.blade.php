<!-- Pypal Credentials -->
<div class="modal fade" id="paypalCredentials" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="paypalCredentials">Paypal Credentials</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <table class="table table-striped">
                <tr>
                    <th>Account Type</th>
                    <th>Email</th>
                    <th>Password</th>
                </tr>

                <tbody>
                    <tr>
                        <td>Personal Account</td>
                        <td>sb-nr47cw3483696@personal.example.com</td>
                        <td>{{'7.<C.jWX'}}</td>

                    </tr>
                    <tr>
                        <td>Business Account</td>
                        <td>sb-tp0so3486552@business.example.com</td>
                        <td>{{'n"*nC|B7'}}</td>

                    </tr>
                </tbody>
            </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

<!-- Stripe Credentials --->
  <div class="modal fade" id="stripeCredentials" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="stripeCredentials">Stripe Credentials</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <table class="table table-striped">
                <tbody>
                    <tr>
                        <th>SCA Activated Card</th>
                    </tr>
                    <tr>
                        <td>4000000000003220</td>
                    </tr>
                    <tr>
                        <th>General Card</th>
                    </tr>
                    <tr>
                        <td>4242424242424242</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>




<!-- Payeezy Credentials --->
<div class="modal fade" id="payeezyCredentials" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="payeezyCredentials">Payeezy Credentials</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <table class="table table-striped">
                <tbody>
                    <tr>
                        <th>Card Number</th>
                    </tr>
                    <tr>
                        <td>4012000033330026 / 4788250000028291</td>
                    </tr>
                    <tr>
                        <th>Expiry Date</th>
                    </tr>
                    <tr>
                        <td>Any future date</td>
                    </tr>
                    <tr>
                        <th>Cvv</th>
                    </tr>
                    <tr>
                        <td>Any Valid digit</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>



  <!-- Payeezy Credentials --->
<div class="modal fade" id="authorizeDotNetCredentials" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="authorizeDotNetCredentials">Authorize.Net Credentials</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <table class="table table-striped">
                <tbody>
                    <tr>
                        <th>Card Number</th>
                    </tr>
                    <tr>
                        <td>4111111111111111</td>
                    </tr>
                    <tr>
                        <th>Expiry Date</th>
                    </tr>
                    <tr>
                        <td>Any future date (YYYY-MM)</td>
                    </tr>
                    <tr>
                        <th>Cvv</th>
                    </tr>
                    <tr>
                        <td>Any Valid digit</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>



  <!--- Payeezy Payment Form --->
  <div class="modal fade" id="payeezyPayentForm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
            @php $segment = Request::segment(1); @endphp
          <h5 class="modal-title" id="payeezyPayentForm">@if($segment == 'authorize-dot-net') Authorize Credentials @endif @if($segment == 'payeezy-integration') Payeezy Credentials @endif</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <!-- CREDIT CARD FORM STARTS HERE -->
            <div class="panel panel-default credit-card-box">
                <div class="panel-heading display-table" >
                   <div class="row display-tr " >
                      <h3 class="panel-title display-td" >Payment Details</h3>
                   </div>
                </div>
                <div class="panel-body">
                    @if($segment == 'authorize-dot-net') <form action="{{url('authorize-dot-net')}}" method="POST"> @endif
                    @if($segment == 'payeezy-integration') <form action="{{url('payeezy-integration')}}" method="POST"> @endif
                    @csrf
                      <div class="row">
                         <div class="col-xs-12">
                            <div class="form-group">
                               <label for="cardNumber">CARD NUMBER</label>
                               <div class="input-group">
                                  <input
                                     type="tel"
                                     class="form-control"
                                     id="cardNumber"
                                     name="cardNumber"
                                     placeholder="Valid Card Number"
                                     autocomplete="cc-number"
                                     required autofocus
                                     />
                                  <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                               </div>
                            </div>
                         </div>
                      </div>
                      <div class="row">
                         <div class="col-xs-7 col-md-7">
                            <div class="form-group">
                               <label for="cardExpiry"><span class="hidden-xs">EXPIRATION</span><span class="visible-xs-inline">EXP</span> DATE</label>
                               <input
                                  type="tel"
                                  class="form-control"
                                  id="cardExpiry"
                                  name="cardExpiry"
                                  placeholder="MM / YY"
                                  autocomplete="cc-exp"
                                  required
                                  />
                            </div>
                         </div>
                         <div class="col-xs-5 col-md-5 pull-right">
                            <div class="form-group">
                               <label for="cardCVC">CV CODE</label>
                               <input
                                  type="tel"
                                  class="form-control"
                                  id="cardCVC"
                                  name="cardCVC"
                                  placeholder="CVC"
                                  autocomplete="cc-csc"
                                  required
                                  />
                            </div>
                         </div>
                      </div>
                      <div class="row">
                         <div class="col-xs-12">
                            <button class="btn btn-success btn-lg btn-block" type="submit">Confirm Order</button>
                         </div>
                      </div>
                      <div class="row" style="display:none;">
                         <div class="col-xs-12">
                            <p class="payment-errors"></p>
                         </div>
                      </div>
                   </form>
                </div>
             </div>
             <!-- CREDIT CARD FORM ENDS HERE -->
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

 <!--Stripe Without Element --->
 <div class="modal fade" id="stripeWithoutElement" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="stripeWithoutElement">Stripe Card Detail</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <!-- CREDIT CARD FORM STARTS HERE -->
            <div class="panel panel-default credit-card-box">
                <div class="panel-heading display-table" >
                   <div class="row display-tr " >
                      <h3 class="panel-title display-td" >Payment Details</h3>
                   </div>
                </div>
                <div class="panel-body">
                <form action="{{url('stripe-without-element')}}" method="POST">
                    @csrf
                      <div class="row">
                         <div class="col-xs-12">
                            <div class="form-group">
                               <label for="cardNumber">CARD NUMBERR</label>
                               <div class="input-group">
                                  <input
                                     type="tel"
                                     class="form-control cardNumber"
                                     id="cardNumber"
                                     name="cardNumber"
                                     placeholder="Valid Card Number"
                                     autocomplete="cc-number"
                                     required autofocus
                                     />
                                  <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                               </div>
                            </div>
                         </div>
                      </div>
                      <div class="row">
                         <div class="col-xs-7 col-md-7">
                            <div class="form-group">
                               <label for="cardExpiry"><span class="hidden-xs">EXPIRATION</span><span class="visible-xs-inline">EXP</span> DATE</label>
                               <input
                                  type="tel"
                                  class="form-control cardExpiry"
                                  id="cardExpiry"
                                  name="cardExpiry"
                                  placeholder="MM / YY"
                                  autocomplete="cc-exp"
                                  required
                                  />
                            </div>
                         </div>
                         <div class="col-xs-5 col-md-5 pull-right">
                            <div class="form-group">
                               <label for="cardCVC">CV CODE</label>
                               <input
                                  type="tel"
                                  class="form-control cardCVC"
                                  id="cardCVC"
                                  name="cardCVC"
                                  placeholder="CVC"
                                  autocomplete="cc-csc"
                                  required
                                  />
                            </div>
                         </div>
                      </div>
                      <div class="row">
                         <div class="col-xs-12">
                            <button class="btn btn-success btn-lg btn-block" type="submit">Confirm Order</button>
                         </div>
                      </div>
                      <div class="row" style="display:none;">
                         <div class="col-xs-12">
                            <p class="payment-errors"></p>
                         </div>
                      </div>
                   </form>
                </div>
             </div>
             <!-- CREDIT CARD FORM ENDS HERE -->
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
<!--- Paypal Form ---->
<div class="modal fade" id="paypalForm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="paypalForm">Paypal</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <!-- CREDIT CARD FORM STARTS HERE -->
            <div class="panel panel-default credit-card-box">
                <div class="panel-heading display-table" >
                   <div class="row display-tr " >
                      <h3 class="panel-title display-td" >Paypal</h3>
                   </div>
                </div>
                <div class="panel-body">
                    <div class="card">
                        <div class="card-body">
                            <form id="payment-form">
                                <div class="form-group">
                                    <input type="text" id="name" name="name" class="form-control" placeholder="Name">
                                </div>
                                <div class="form-group">
                                    <input type="email" id="email" name="email" class="form-control" placeholder="Email">
                                </div>
                                <div class="form-group">
                                    <input type="text" id="phone" name="phone" class="form-control" placeholder="Phone">
                                </div>
                                <div class="form-group">
                                    <select name="country" id="country" class="form-control">
                                        <option value="">Select Country</option>
                                        <option value="pakistan">Pakistan</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <select name="state" id="state" class="form-control">
                                        <option value="">Select State</option>
                                        <option value="sindh">Sindh</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <select name="city" id="city" class="form-control">
                                        <option value="">Select City</option>
                                        <option value="karachi">Karachi</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <input type="number" id="zip" name="zip" class="form-control" placeholder="Zip">
                                </div>
                                <div class="form-group">
                                    <input type="text" id="address" name="address" class="form-control" placeholder="Address">
                                </div>
                                <div class="form-group">
                                    <div id="card-element"></div>
                                    <!-- We'll put the error messages in this element -->
                                    <div id="card-errors" role="alert"></div>
                                </div>
                            </form>
                            <div id="paypal-button-container"></div>
                        </div>
                    </div>
                </div>
             </div>
             <!-- CREDIT CARD FORM ENDS HERE -->
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
