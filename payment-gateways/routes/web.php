<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PaypalController;
use App\Http\Controllers\StripeController;
use App\Http\Controllers\PayeezyController;
use App\Http\Controllers\AuthorizeDotNetConroller;
use App\Http\Controllers\GooglePayController;
use App\Http\Controllers\ApplePayController;
use App\Http\Conrollers\AliPayController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//*******************************************************************************/
//                                    Paypal
//*******************************************************************************/
Route::get('paypal', [PaypalController::class, 'paypal']);
Route::get('/paypal-client-side', [PaypalController::class, 'paypalClientSide']);
Route::get('/paypal-server-side', [PaypalController::class, 'paypalServerSide']);
Route::post('paypal-server-response-from-client-side', [PaypalController::class, 'paypalServerResponseFromClientSide']);
Route::post('paypal-server-response-from-server-side', [PaypalController::class, 'paypalServerResponseFromServerSide']);
//
//*******************************************************************************/
//                                    Stripe
//*******************************************************************************/
Route::get('stripe', [StripeController::class, 'stripe']);
Route::get('stripe-sca-customer-payment-flow-client-side', [StripeController::class, 'stripeSCAClientSide']);
Route::get('stripe-sca-customer-payment-flow-server-side', [StripeController::class, 'stripeSCAServerSide']);
Route::post('stripe-cpf-response-client-side', [StripeController::class, 'stripeCPFResponseClientSide']);
Route::post('stripe-cpf-response-server-side', [StripeController::class, 'stripeCPFResponseServerSide']);
Route::get('stripe-sca-pre-built-checkout', [StripeController::class, 'stripeSCAPreBuiltCheckout']);
Route::post('create-session', [StripeController::class, 'createSession']);
Route::get('stripe-success', [StripeController::class, 'stripeSuccess']);
Route::get('stripe-failed', [StripeController::class, 'stripeFailed']);
Route::get('stripe-without-element', [StripeController::class, 'stripeWithoutElement']);
Route::post('stripe-without-element', [StripeController::class, 'stripeWithoutElementSave']);

//
//*******************************************************************************/
//                                    Payeezy
//*******************************************************************************/
Route::get('payeezy', [PayeezyController::class, 'payeezy']);
Route::get('payeezy-integration', [PayeezyController::class, 'payeezyIntegration']);
Route::post('payeezy-integration', [PayeezyController::class, 'payeezyIntegrationResponse']);
//
//*******************************************************************************/
//                                    Authorize.net
//*******************************************************************************/
Route::get('authorize', [AuthorizeDotNetConroller::class, 'index']);
Route::get('authorize-dot-net', [AuthorizeDotNetConroller::class, 'authorizeDotNet']);
Route::post('authorize-dot-net', [AuthorizeDotNetConroller::class, 'authorizeDotNetResponse']);
//
//*******************************************************************************/
//                                    Google Pay
//*******************************************************************************/
Route::get('google-pay', [GooglePayController::class, 'googlePay']);
//
//*******************************************************************************/
//                                    Apple Pay
//*******************************************************************************/
Route::get('apple-pay', [ApplePayController::class, 'applePay']);
Route::get('ali-pay', [ApplePayController::class, 'aliPay']);

//
//*******************************************************************************/
//                                    Ali Pay
//*******************************************************************************/
Route::get('ali-payy', [AliPayController::class, 'aliPay']);


Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

