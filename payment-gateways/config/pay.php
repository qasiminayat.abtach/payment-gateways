<?php

return [
    'alipay' => [
        // APPID assigned by Alipay
        'app_id' => env('ALI_APP_ID','SANDBOX_5Y0A7N2Y4L4U05645'),

        // Alipay asynchronous notification address
        'notify_url' =>'http://payment-integraion.local/ali-pay',

        // Sync notification address after successful payment
        'return_url' =>'http://payment-integraion.local',

        // Ali public key, used when verifying the signature
        'ali_public_key' => env('ALI_PUBLIC_KEY','MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArEsr2vx65kFzC/7BK6+Mz57h4vZojefnh1ZtLOwSwKk/VjKHnK6/sUY5mZaKQuJY7gUKRElZJamjLmqQD0Z/Brn39M+O1EDl279VlZGbFHLmGPBlc4eNEWgFOx/XzWGDemR0CPJcMOT9Xf5Q383AlWdNt2mLbiwJFSqqZdMUp7X8B687tW35YudF7yr3BK4oEXgFbQkOZByW2Fpg31PL7xXhpdCo3yccT/3pG5BH8tG8raioME9HNgLK2Li6Cv5yuJoXMSV57LCwREDOanuGtxSyNplihVMYQwWC3lTHbl5fpu0QEcXv+QlXwmHpuIIVE80TtoZuV3xQ8tDc5U1fpQIDAQAB'),

        // own private key, used when signing
        'private_key' => env('ALI_PRIVATE_KEY','MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCsSyva/HrmQXML/sErr4zPnuHi9miN5+eHVm0s7BLAqT9WMoecrr+xRjmZlopC4ljuBQpESVklqaMuapAPRn8Guff0z47UQOXbv1WVkZsUcuYY8GVzh40RaAU7H9fNYYN6ZHQI8lww5P1d/lDfzcCVZ023aYtuLAkVKqpl0xSntfwHrzu1bfli50XvKvcErigReAVtCQ5kHJbYWmDfU8vvFeGl0KjfJxxP/ekbkEfy0bytqKgwT0c2AsrYuLoK/nK4mhcxJXnssLBEQM5qe4a3FLI2mWKFUxhDBYLeVMduXl+m7RARxe/5CVfCYem4ghUTzRO2hm5XfFDy0NzlTV+lAgMBAAECggEBAJ+BpGNDNYBv/VOQkUOhbWpVfGiDmT4vARExeTeKgr3ssl9ZvmTW/07eZbsEPJI5PP12mw+nHCobB6CUNTR/HmCa0KzLbz6fqm2rkE89EuZ2jQFvpWy0hGwl8hnPnBle+rrXvvj9b04w4UQgirz3niBzF+cChnwDtpDrA//q0AeGj/Xz7pfypWXM5G0ZLNcO9pYulyhfkVxKvy2QtiikmT1EoDNcxZmw6EoZPJLdKKX0Zwh7F9fWOyV1xuj8QbR3/Fp0n/aMmICf2cjsdHddg6XiuBjKNloQqh7P+C2OEVxZRmOFG+vGSPygb0AwM/lMb4gNrB92tMaTwpsAzvfyDiECgYEA+R9hDv6cXldN+I5Q/m4Ezo0Pt5WwXhrhEzd7rAlvdoYvhv9oSqUElJye6xZ1V7CeZOD1TuPgO/+7iYRQzz7s3qY+/S4fWbrUdtlnItHVYKZ1mUNIdnkUVmR4kEjO1JRVxzdzvjDaza6IcgH+XQ4l5dOxUdgr6DL+yVYXG2SBpxkCgYEAsQzR6sIHTtVuAsjfXRaGdQ1Q2f7CrOLIQKL50i8KBoaKj3WXDQ7FRSUQLBLYMvUaxQN0VOTU78vKfN2XxAstuwf+Rjz6edeXF8ghY5TTjY4h87EIfxpyhx8X64bxwvXevrOVqMbA5ShKlAUxFM34N2rAn39L/MOMe3hOhpOsSm0CgYAbcuap1Un7ZxkzOtMXxZ2H+KjzrG2bYNDN4j4hLYT3hT91W2ztVJNVoJAuDPJu8AnzyYmd4KPTWJEcTNTukNteiOjF0rAnndtNIRUpE/+zv4OTmINCcoLYJgfWtf78E0AETErXeOCS6xkjnq6dKDLqyNFv2Ca2VfAW1QKExSU3+QKBgQCs9Q8kPIOTGaCsi7rYXybetjFjpCJdbfWeXxeZomYy1cAKQR9cnC9OStSvmYEm1pZeG+/K9vilgPnAXWDjHOCBs19NEkjGOjvZ+aHUfBDmWhEE1M6uDBIRuehOJ7eJc1M0DI+JHxBVTLT+QMwfvM5cpKajqWF7/TQBcheUFDLdSQKBgC2dF3fKcx2ZKtvAjgS+1CtTIQNccw3ZbacwSK1Zty7e8PHywI/aY/NIAvFkxCghy0GWzuhg8Z/Jxw61UViM4ediuxx03LiNC2q8i1UUjwJNf4QKCKdibm0H6EhaAtgsY+w+6uZCSbQVMxUhFS31mwmlEcmr31YyQvLyItGPCh1b'),

        // To use the public key certificate mode, please configure the following two parameters and modify ali_public_key to the Alipay public key certificate path ending with .crt, such as (./cert/alipayCertPublicKey_RSA2.crt)
        // Application public key certificate path
        //'app_cert_public_key' =>'./cert/appCertPublicKey.crt',

        // Alipay root certificate path
        //'alipay_root_cert' =>'./cert/alipayRootCert.crt',

        // optional, default warning; log path: sys_get_temp_dir().'/logs/yansongda.pay.log'
        'log' => [
            'file' => storage_path('logs/alipay.log'),
        //'level' =>'debug'
        //'type' =>'single', // optional, optional daily.
        //'max_file' => 30,
        ],

        // optional, setting this parameter will enter the sandbox mode
        //'mode' =>'dev',
    ],

    'wechat' => [
        // Official account APPID
        'app_id' => env('WECHAT_APP_ID',''),

        // applet APPID
        'miniapp_id' => env('WECHAT_MINIAPP_ID',''),

        // appid referenced by APP
        'appid' => env('WECHAT_APPID',''),

        // WeChat merchant account assigned by WeChat Pay
        'mch_id' => env('WECHAT_MCH_ID',''),

        // WeChat payment asynchronous notification address
        'notify_url' =>'',

        // WeChat payment signature key
        'key' => env('WECHAT_KEY',''),

        // Client certificate path, refund, red envelope, etc. need to be used. Please fill in the absolute path, linux please ensure that the permission is wrong. pem format.
        'cert_client' =>'',

        // Client secret key path, refund, red envelope, etc. need to be used. Please fill in the absolute path, linux please ensure that the permission is wrong. pem format.
        'cert_key' =>'',

        // optional, default warning; log path: sys_get_temp_dir().'/logs/yansongda.pay.log'
        'log' => [
            'file' => storage_path('logs/wechat.log'),
        //'level' =>'debug'
        //'type' =>'single', // optional, optional daily.
        //'max_file' => 30,
        ],

        // optional
        // Sandbox mode when'dev'
        //'hk' is a Southeast Asian node
        //'mode' =>'dev',
    ],
];
